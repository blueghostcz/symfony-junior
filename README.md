PHP/Symfony junior programátor
==============================

V tomto dokumentu naleznete zadání jednoduchého implementačního testu pro
uchazeče na pozici PHP/Symfony programátor. Celkem půjde o napsání nějakých
entit a základními operacemi s nimi. Postačí tedy základní znalosti
programování a návrhu.

V repozitáři na [Bitbucketu][1] naleznete mimo jiné kostru projektu v Symfony 2.8
ve které byste měli dodělat následující úkoly. Repozitář si naklonujte a
vytvořte si větev s názvem ve formátu „prijmeni-jmeno“. Během práce dle
vlastního uvážení průběžně commitujte dílčí části a nakonec vytvořte pull
request z Vaší větve do masteru.

Zadání
------

Vytvořte jednoduchý systém správy školních předmětů s ohledem na přehled učitelů a jejich předmětů. 
Systém by měl obsahovat pouze jednoduchý výpis všech učitelů s předměty, které učí a administraci učitelů a předmětů. 

Na frontendu se bude funkcionalita chovat takto:

1. Stránka výpisu
    1. Bude obsahovat všechny učitele a u každého učitele seznam předmětů, které učí (tabulka)
2. Editace učitele
    1. Bude obsahovat formulář pro editaci/vytvoření nového učitele a všech jeho vlastností
        1. Jméno
        2. Příjmení 
3. Editace předmětu
    1. Bude obsahovat formulář pro editaci/vytvoření nového předmětu a všech vlastností
        1. Kód
        2. Název
        3. Vyučujícího (musí být zadán)

[1]:  https://bitbucket.org/blueghostcz/symfony-junior